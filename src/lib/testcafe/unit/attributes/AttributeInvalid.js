export default class AttributeInvalidNUnitTests {

    constructor( componentModel ) {
        this._model = componentModel;
    }

    runAllTests() {
        fixture( this._model.componentName + ' All Attribute Invalid Unit Tests' )
            .page( this._model.testServer );

        this._runAttributeInvalidSBUnitTests();
        this._runAttributeInvalidNUnitTests();
    }

    runAttributeInvalidNUnitTests() {
        fixture( this._model.componentName + ' Number Attribute Invalid Unit Tests' )
            .page( this._model.testServer );

        this._runAttributeInvalidNUnitTests();
    }

    runAttributeInvalidSBUnitTests() {
        fixture( this._model.componentName + ' String & Boolean Attribute Invalid Unit Tests' )
            .page( this._model.testServer );

        this._runAttributeInvalidSBUnitTests();
    }    

    _runAttributeInvalidNUnitTests() {     
         for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            if( prop.type === Number ) {
                test( 'Unit Tests - Attributes - ' + prop.attributeName + ' - number - invalid types', async t => {
                    let expected = await this._model.component[prop.name];
                    expected = expected.toString(); 
                    await this._model.setComponentAttribute( t, prop.attributeName, 'test' );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" - set \'test\' - invalid string does not update' );
                    await this._model.setComponentAttribute( t, prop.attributeName, null );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" - set null - null does not update' );
                    await this._model.setComponentAttribute( t, prop.attributeName, {} );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" - set {} - object does not update' );
                    await this._model.setComponentAttribute( t, prop.attributeName, [] );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" - set [] - array does not update' );
                    await this._model.setComponentAttribute( t, prop.attributeName, undefined );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" - set undefined - undefined does not update' );                       
                } );
            }
        }
    }
    
    _runAttributeInvalidSBUnitTests() {
         for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            if( prop.type === String || prop.type === Boolean ) {
                let type = 'string';
                if( prop.type === Boolean ) type = 'boolean';
                test( 'Unit Tests - Attributes - ' + prop.attributeName + ' - ' + type + ' - invalid types', async t => {
                    let expected = await this._model.component[prop.name];
                    expected = expected.toString();
                    this._testAttribute( t, prop, expected, 1, 'number' );
                    this._testAttribute( t, prop, expected, {}, 'object' );
                    this._testAttribute( t, prop, expected, [], 'array' );
                    this._testAttribute( t, prop, expected, undefined, 'undefined' );
                    if( prop.type === Boolean ) {
                        await this._model.setComponentAttribute( t, prop.attributeName, 'test' );
                        if( expected === 'true' ) {
                            await this._model.testAttributeExists( t, prop.attributeName, 'Attribute "' + prop.attributeName + '" - set \'test\' - invalid string does not update' );
                        } else {
                            await this._model.testAttributeNotExists( t, prop.attributeName, 'Attribute "' + prop.attributeName + '" - set \'test\' - invalid string does not update' );
                        }
                    } else {
                        await this._model.setComponentAttribute( t, prop.attributeName, true );
                        if( expected !== 'null' ) {
                            await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" - set true - boolean does not update' );
                        } else {
                            await this._model.testAttributeNotExists( t, prop.attributeName, 'Attribute "' + prop.attributeName + '" - set true - boolean does not update' );
                        }
                    }                               
                } );
            }
        }
    }

    async _testAttribute( t, prop, expected, setTo, testType ) {
        await this._model.setComponentAttribute( t, prop.attributeName, setTo );
        let message = 'Attribute "' + prop.attributeName + '" - set ' + setTo + ' - ' + testType + ' does not update'
        if( this._checkNotExisted( prop.type, expected ) ) {
            await this._model.testAttributeNotExists( t, prop.attributeName, message );
        } else {
            if( prop.type === Boolean ) {
                await this._model.testAttributeExists( t, prop.attributeName, message );
            } else {
                await this._model.testAttributeValue( t, prop.attributeName, expected, message );
            }                                
        }
    }

    _checkNotExisted( type, expected ) {
        return ( type === Boolean && expected === 'false' ) || ( type === String && expected === 'null' )
    }
}