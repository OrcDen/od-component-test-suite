export default class AttributeDefaultUnitTests {

    constructor( componentModel ) {
        this._model = componentModel;
    }

    runAllTests() {
        this._runAttributeDefaultsUnitTests();
    }

    runAttributeDefaultsUnitTests() {
        this._runAttributeDefaultsUnitTests();
    }

    _runAttributeDefaultsUnitTests() {
        fixture( this._model.componentName + ' All Attribute Defaults Unit Tests' )
            .page( this._model.testServer );

        for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            switch ( prop.type ) {
                case Boolean:
                    this._testBoolean( prop.attributeName, prop.default.toString() );
                    break;
                case Number: 
                    this._testNumber( prop.attributeName, prop.default.toString() );
                    break;
                case String:
                    this._testString( prop.attributeName, prop.default );                        
                    break;
                default:
                    break;
            }
        }
    }

    _testBoolean( name, defaultV ) {
        test( 'Unit Tests - Attributes - ' + name + ' - boolean - defaults', async t => {    
            if( defaultV === 'true' ) {
                await this._model.testAttributeExists( t, name, 'Attribute "' + name + '" default - present' );
            } else {
                await this._model.testAttributeNotExists( t, name, 'Attribute "' + name + '" default - not present' );
            }
        } );
    }

    _testNumber( name, defaultV ) {
        test( 'Unit Tests - Attributes - ' + name + ' - number - defaults', async t => {    
            await this._model.testAttributeValue( t, name, defaultV, 'Attribute "' + name + '" default ' + defaultV );
        } );
    }

    _testString( name, defaultV ) {
        test( 'Unit Tests - Attributes - ' + name + ' - string - defaults', async t => {    
            if( defaultV !== null ) {
                await this._model.testAttributeValue( t, name, defaultV, 'Attribute "' + name + '" default ' + defaultV );
            } else {
                await this._model.testAttributeNotExists( t, name, 'Attribute "' + name + '" default - not present' );
            }
        } );
    }
}

