export default class AttributeBehaviourUnitTests {

    constructor( componentModel ) {
        this._model = componentModel;
    }

    runAllTests() {
        this._runAttributeBehaviourUnitTests();
    }

    runAttributeBehaviourUnitTests() {
        this._runAttributeBehaviourUnitTests();
    }

    _runAttributeBehaviourUnitTests() {
        fixture( this._model.componentName + ' All Attribute Behaviour Unit Tests' )
            .page( this._model.testServer );

        for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            switch ( prop.type ) {
                case Boolean:
                    this._testBoolean( prop.attributeName, prop.default );
                    break;
                case String:
                    this._testString( prop.attributeName, prop.validValue, prop.default );
                    break;
                case Number: 
                    this._testNumber( prop.attributeName, prop.validValue, prop.default );
                    break;
                default:
                    break;
            }
        }
    }

    _testBoolean( name, defaultV ) {
        test( 'Unit Tests - Attributes - ' + name + ' - boolean - behaviour', async t => {
            await this._model.setComponentAttribute( t, name, true );
            await this._model.testAttributeExists( t, name, 'Attribute "' + name + '" - accepts true' );
            await this._model.setComponentAttribute( t, name, false );
            await this._model.testAttributeNotExists( t, name, 'Attribute "' + name + '" - accepts false - attribute not present' );
            await this._model.setComponentAttribute( t, name, 'true' );
            await this._model.testAttributeExists( t, name, 'Attribute "' + name + '" - accepts \'true\'' );
            await this._model.setComponentAttribute( t, name, 'false' );
            await this._model.testAttributeNotExists( t, name, 'Attribute "' + name + '" - accepts \'false\' - attribute not present' );
            await this._model.setComponentAttribute( t, name, defaultV );
        } );               
    }

    _testString( name, valid, defaultV ) {
        test( 'Unit Tests - Attributes - ' + name + ' - string - behaviour', async t => {
            await this._model.setComponentAttribute( t, name, valid );
            await this._modal.testAttributeValue( t, name, valid, 'Attribute "' + name + '" - accepts \'' + valid + '\'' );
            await this._model.setComponentAttribute( t, name, null );
            await this._model.testAttributeNotExists( t, name, 'Attribute "' + name + '" - accepts null - attribute not present' );
            await this._model.setComponentAttribute( t, name, valid );
            await this._model.setComponentAttribute( t, name, 'null' );
            await this._model.testAttributeNotExists( t, name, 'Attribute "' + name + '" - accepts \'null\' - attribute not present' );          
            await this._model.setComponentAttribute( t, name, defaultV );
        } );              
    }

    _testNumber( name, valid, defaultV ) {
        test( 'Unit Tests - Attributes - ' + name + ' - number - behaviour', async t => {
            await this._model.setComponentAttribute( t, name, valid );
            await this._model.testAttributeValue( t, name, valid.toString(), 'Attribute "' + name + '" - accepts ' + valid );    
            await this._model.setComponentAttribute( t, name, defaultV );
            await this._model.setComponentAttribute( t, name, valid.toString() );
            await this._model.testAttributeValue( t, name, valid.toString(), 'Attribute "' + name + '" - accepts \'' + valid + '\'' );    
            await this._model.setComponentAttribute( t, name, defaultV );
        } );              
    }
}