export default class PropertyBehaviourUnitTests {

    constructor( componentModel ) {
        this._model = componentModel;
    }

    runAllTests() {
        this._runPropertyBehaviourUnitTests();
    }

    runPropertyBehaviourUnitTests() {
        this._runPropertyBehaviourUnitTests();
    }

    _runPropertyBehaviourUnitTests() {
        fixture( this._model.componentName + ' All Property Behaviour Unit Tests' )
            .page( this._model.testServer );

        for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            switch ( prop.type ) {
                case Boolean:
                    this._testBoolean( prop.name, prop.default );
                    break;
                case String:
                    this._testString( prop.name, prop.validValue, prop.default );
                    break;
                case Number: 
                    this._testNumber( prop.name, prop.validValue, prop.default );
                    break;
                default:
                    break;
            }
        }
    }

    _testBoolean( name, defaultV ) {
        test( 'Unit Tests - Properties - ' + name + ' - boolean - behaviour', async t => {
            await this._model.setComponentProperty( t, name, true );
            await this._model.testPropertyValue( t, name, true, 'Property "' + name + '" - accepts true' );
            await this._model.setComponentProperty( t, name, false );
            await this._model.testPropertyValue( t, name, false, 'Property "' + name + '" - accepts false' );
            await this._model.setComponentProperty( t, name, defaultV );
        } );               
    }

    _testString( name, valid, defaultV ) {
        test( 'Unit Tests - Properties - ' + name + ' - string - behaviour', async t => {
            await this._model.setComponentProperty( t, name, valid );
            await this._model.testPropertyValue( t, name, valid, 'Property "' + name + '" - accepts \'' + valid + '\'' );
            await this._model.setComponentProperty( t, name, null );
            await this._model.testPropertyValue( t, name, null, 'Property "' + name + '" - accepts null' ); 
            await this._model.setComponentProperty( t, name, valid );  
            await this._model.setComponentProperty( t, name, 'null' );
            await this._model.testPropertyValue( t, name, null, 'Property "' + name + '" - accepts \'null\'' );        
            await this._model.setComponentProperty( t, name, defaultV );
        } );              
    }

    _testNumber( name, valid, defaultV ) {
        test( 'Unit Tests - Attributes - ' + name + ' - number - behaviour', async t => {
            await this._model.setComponentProperty( t, name, valid );
            await this._model.testPropertyValue( t, name, valid, 'Property "' + name + '" - accepts ' + valid );    
            await this._model.setComponentProperty( t, name, defaultV );
        } );              
    }
}