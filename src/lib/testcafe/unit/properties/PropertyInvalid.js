export default class PropertyInvalidUnitTests {

    constructor( componentModel ) {
        this._model = componentModel;
    }

    runAllTests() {
        this._runPropertyInvalidUnitTests();
    }

    runPropertyInvalidUnitTests() {
        this._runPropertyInvalidUnitTests();
    }

    _runPropertyInvalidUnitTests() {
        fixture( this._model.componentName + ' All Property Invalid Unit Tests' )
            .page( this._model.testServer );

        for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            let type = 'string';
            if( prop.type === Boolean ) type = 'boolean';
            if( prop.type === Number ) type = 'number';
            test( 'Unit Tests - Properties - ' + prop.name + ' - ' + type + ' - invalid types', async t => {
                if( prop.type !== Object ) {
                    await this._model.setComponentProperty( t, prop.name, {} );
                    await this._model.testPropertyValue( t, prop.name, prop.default, 'Property "'+ prop.name +'" - set {} - object does not update' );                
                }
                if( prop.type !== Array ) {
                    await this._model.setComponentProperty( t, prop.name, [] );
                    await this._model.testPropertyValue( t, prop.name, prop.default, 'Property "'+ prop.name +'" - set [] - array does not update' );
                }
                if( prop.type !== Boolean ) {
                    await this._model.setComponentProperty( t, prop.name, true );
                    await this._model.testPropertyValue( t, prop.name, prop.default, 'Property "'+ prop.name +'" - set true - boolean does not update' );
                }
                if( prop.type !== String ) {
                    await this._model.setComponentProperty( t, prop.name, 'test' );
                    await this._model.testPropertyValue( t, prop.name, prop.default, 'Property "'+ prop.name +'" - set \'test\' - invalid string does not update' );
                }
                if( prop.type !== Number ) {
                    await this._model.setComponentProperty( t, prop.name, 1 );
                    await this._model.testPropertyValue( t, prop.name, prop.default, 'Property "'+ prop.name +'" - set 1 - number does not update' );
                }
                if( prop.type !== Object && prop.type !== Array && prop.type !== String ) {
                    await this._model.setComponentProperty( t, prop.name, null );
                    await this._model.testPropertyValue( t, prop.name, prop.default, 'Property "'+ prop.name +'" - set null - null does not update' );
                }
                if( prop.type === Boolean ) {
                    let invert = !prop.default;
                    invert = invert.toString();
                    await this._model.setComponentProperty( t, prop.name, invert );
                    await this._model.testPropertyValue( t, prop.name, prop.default, 'Property "'+ prop.name +'" - set \'' + invert + '\' - valid string does not update' );
                }
                if( prop.type === Number ) {
                    let test = prop.validValue.toString();
                    await this._model.setComponentProperty( t, prop.name, test );
                    await this._model.testPropertyValue( t, prop.name, prop.default, 'Property "'+ prop.name +'" - set \'' + test + '\' - valid string does not update' );
                }
                await this._model.setComponentProperty( t, prop.name, undefined );
                await this._model.testPropertyValue( t, prop.name, prop.default, 'Property "'+ prop.name +'" - set undefined - undefined does not update' );
            } );
        }
    }
}