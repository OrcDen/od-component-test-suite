export default class PropertyDefaultsUnitTests {

    constructor( componentModel ) {
        this._model = componentModel;
    }

    runAllTests() {
        this._runPropertyDefaultsUnitTests();
    }

    runPropertyDefaultsUnitTests() {
        this._runPropertyDefaultsUnitTests();
    }

    _runPropertyDefaultsUnitTests() {
        fixture( this._model.componentName + ' All Property Defaults Unit Tests' )
            .page( this._model.testServer );

        for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            let type = 'string';
            if( prop.type === Boolean ) type = 'boolean';
            if( prop.type === Number ) type = 'number';
            test( 'Unit Tests - Properties - ' + prop.name + ' - ' + type + ' - Defaults', async t => {              
                await this._model.testPropertyValue( t, prop.name, prop.default, 'Property "' + prop.name + '" default ' + prop.default );
            } );
        }
    }
}