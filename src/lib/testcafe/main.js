import ComponentModel from './common/ComponentModel';
import CommonWebcomponentsTests from './common/CommonComponent';
import PropertyDefaultsUnitTests from './unit/properties/PropertyDefaults';
import AttributeDefaultsUnitTests from './unit/attributes/AttributeDefaults';
import PropertyInvalidUnitTests from './unit/properties/PropertyInvalid';
import AttributeInvalidUnitTests from './unit/attributes/AttributeInvalid';
import PropertyBehaviourUnitTests from './unit/properties/PropertyBehaviour';
import AttributeBehaviourUnitTests from './unit/attributes/AttributeBehaviour';
import AttributePropertyInvalidIntegrationTests from './integration/AttributePropertyInvalid';
import AttributePropertyBehaviourIntegrationTests from './integration/AttributePropertyBehaviour';

export default class TestCafeComponentTestSuite {

    constructor( config ) {
        this._model = new ComponentModel( config );
        this._commonComponentsTests = new CommonWebcomponentsTests( this.model );
        this._propertyDefaultsUnitTests = new PropertyDefaultsUnitTests( this.model );
        this._attributeDefaultsUnitTests = new AttributeDefaultsUnitTests( this.model );
        this._propertyInvalidUnitTests = new PropertyInvalidUnitTests( this.model );
        this._attributeInvalidUnitTests = new AttributeInvalidUnitTests( this.model );
        this._propertyBehaviourUnitTests = new PropertyBehaviourUnitTests( this.model );
        this._attributeBehaviourUnitTests = new AttributeBehaviourUnitTests( this.model );        
        this._attributePropertyInvalidIntegrationTests = new AttributePropertyInvalidIntegrationTests( this.model );
        this._attributePropertyBehaviourIntegrationTests = new AttributePropertyBehaviourIntegrationTests( this.model );
    }

    get model() {
        return this._model;
    }

    get commonComponentTests() {
        return this._commonComponentsTests;
    }

    get propertyDefaultsUnitTests() {
        return this._propertyDefaultsUnitTests;
    }

    get attributeDefaultsUnitTests() {
        return this._attributeDefaultsUnitTests;
    }

    get propertyInvalidUnitTests() {
        return this._propertyInvalidUnitTests;
    }

    get attributeInvalidUnitTests() {
        return this._attributeInvalidUnitTests;
    }

    get propertyBehaviourUnitTests() {
        return this._propertyBehaviourUnitTests;
    }

    get attributeBehaviourUnitTests() {
        return this._attributeBehaviourUnitTests;
    }

    get attributePropertyInvalidIntegrationTests() {
        return this._attributePropertyInvalidIntegrationTests;
    }

    get attributePropertyBehaviourIntegrationTests() {
        return this._attributePropertyBehaviourIntegrationTests;
    }
    
    runAllTests() {
        this.commonComponentTests.runAllTests();
        if( this._model.properties && this._model.properties.length && this._model.properties.length > 0 ) {
            this.propertyDefaultsUnitTests.runAllTests();
            this.attributeDefaultsUnitTests.runAllTests();
            this.propertyInvalidUnitTests.runAllTests();
            this.attributeInvalidUnitTests.runAllTests();
            this.propertyBehaviourUnitTests.runAllTests(); 
            this.attributeBehaviourUnitTests.runAllTests();            
            this.attributePropertyInvalidIntegrationTests.runAllTests();
            this.attributePropertyBehaviourIntegrationTests.runAllTests();           
        }
    }
}