export default class CommonComponentTests {

    constructor( componentModel ) {
        this._model = componentModel;
    }
    
    runAllTests() {
        fixture( this._model.componentName + ' All Common Webcomponents Tests' )
            .page( this._model.testServer );

        this._runWebcomponentsTests();
        if( !this._model.hasShadow ) {
            return;
        }
        this._runShadowDOMTests();
    }

    runWebcomponentsTests() {
        fixture( this._model.componentName + ' Webcomponents Support Tests' )
            .page( this._model.testServer ); 
                  
        this._runWebcomponentsTests();
    }

    _runWebcomponentsTests() {
        test( 'Webcomponents Support & Imports Correctly', async t => {          
            let classIsDefined = await this._model.getComponentClassIsDefined();
            await t
                .expect( classIsDefined ).notEql( false, this._model.componentTag + ' is defined' );
        } );        
    }

    runShadowDOMTests() {
        if( !this._model.hasShadow ) {
            return;
        }
        fixture( this._model.componentName + 'Shadow DOM Support Tests' )
            .page( this._model.testServer );

        this._runShadowDOMTests();
    }

    _runShadowDOMTests() {
        test( 'Shadow DOM Support', async ( t ) => {
            await t
                .expect( this._model.component.exists ).ok( 'Custom element is available' )
                .expect( this._model.innerElement.exists ).ok( 'HTML is available inside shadow DOM' );
        } );
        
    }
}