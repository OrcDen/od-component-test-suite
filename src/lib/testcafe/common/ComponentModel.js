import { Selector, ClientFunction } from 'testcafe';

export default class ComponentModel {
    constructor( config ) {
        this._componentData = config;
        if( !this._validateConfig() ) {
            console.log( 'ERROR: INVALID CONFIGURATION' );
            return;
        }
        this._component = Selector( this.componentSelector ).addCustomDOMProperties( this._buildCustomProps() );

        let tag = this.componentTag
        this._componentClassIsDefined = ClientFunction( () =>{ customElements.get( tag ) !== undefined; }, { dependencies: { tag } } );

        let component = this.component;
        let shadowSelector = this.shadowSelector;
        this._innerElement = Selector( () => component().shadowRoot.querySelector( shadowSelector ), { dependencies: { component, shadowSelector } } );

        let selector = this.componentSelector;
        this._setComponentProperty = ClientFunction( ( name, value ) => { document.querySelector( selector )[name] = value; }, {dependencies: { selector }} );
        this._setComponentAttribute = ClientFunction( ( name, value ) => { document.querySelector( selector ).setAttribute( name, value ); }, {dependencies: { selector }} );
    }

    //Configs
    get testServer() {
        return this._componentData.testServer;
    }

    get componentName() {
        return this._componentData.name;
    }
    
    get componentTag() {
        return this._componentData.componentTag
    }

    get componentSelector() {
        return this._componentData.componentSelector;
    }

    get hasShadow() {
        return this._componentData.hasShadow;
    }

    get shadowSelector() {
        return this._componentData.shadowSelector;
    }

    get properties() {
        return this._componentData.properties;
    }

    //Selectors
    get component() {
        return this._component;
    }

    get innerElement() {
        return this._innerElement;
    }

    //Client Functions
    async getComponentClassIsDefined() {
        return await this._componentClassIsDefined();
    }

    async setComponentProperty( t, name, value ) {
        let boundC = this._setComponentProperty.with( { boundTestRun: t } );
       await boundC( name, value );
    }

    async setComponentAttribute( t, name, value ) {
        let boundC = this._setComponentAttribute.with( { boundTestRun: t } );
        await boundC( name, value );
    }    

    //Test Methods
    async testAttributeNotExists( t, name, message ) {
        let boundS = this.component.with( { boundTestRun: t } );
        await t
            .expect( boundS.withAttribute( name ).exists ).notOk( message );
    }

    async testAttributeExists( t, name, message ) {
        let boundS = this.component.with( { boundTestRun: t } );
        await t
            .expect( boundS.withAttribute( name ).exists ).ok( message );
    }

    async testAttributeValue( t, name, value, message ) {
        let boundS = this.component.with( { boundTestRun: t } );
        await t
            .expect( boundS.withAttribute( name, value ).exists ).ok( message );
    }

    async testPropertyValue( t, name, expected, message ) {
        let boundS = this.component.with( { boundTestRun: t } );
        let value = boundS[name];
        await t
            .expect( value ).eql( expected, message );
    }

    //Private
    _validateConfig() {
        let valid = true;
        if( !this._validateCommonConfigs() ) {
            valid = false;
        }
        if( !this.properties || !this.properties.length || this.properties.length === 0 ) {
            return valid;
        }
        this.properties.forEach( p => {
            if( !this._validateProperty( p ) ) {
                valid = false;
            }
        } );
        return valid;
    }

    _validateCommonConfigs() {
        let valid = true;
        if( !this.componentName || !this.testServer || !this.componentSelector ) {
            console.log( "ERROR: PLEASE ENSURE CONFIGURATION CONTAINS NAME, SERVER, AND SELECTOR" );
            valid = false;
        }
        if( this.hasShadow && !this.shadowSelector ) {
            console.log( "ERROR: NO SELECTOR PROVIDED FOR SHADOW DOM" );
            valid = false;
        }
        return valid;
    }

    _validateProperty( p ) {
        let valid = true;
        if( !this._validatePropertyCommon( p ) ) {
            valid = false;
        }
        if( !this._validatePropertyDefault( p ) ) {
            valid = false;
        }
        if( !this._validatePropertyValidValues( p ) ) {
            valid = false;
        }
        return valid;
    }

    _validatePropertyCommon( p ) {
        let valid = true;
        if( p.name === undefined || p.type === undefined || p.default === undefined ) {
            console.log( "ERROR: PLEASE ENSURE NAME, TYPE, AND DEFAULT IS PROVIDED FOR PROPERTY: ", p );
            valid = false;
        }        
        if( p.type !== Boolean && p.type !== Number && p.type !== String && p.type !== Array && p.type !== Object ) {
            console.log( "ERROR: INCORRECT TYPE PROVIDED FOR PROPERTY: ", p );
            valid = false;
        }
        return valid;
    }

    _validatePropertyDefault( p ) {
        let valid = true;
        switch ( p.type ) {
            case Number:
                if( typeof p.default !== 'number' ) {
                    console.log( "ERROR: DEFAUL SHOULD BE A NUMBER FOR PROPERTY: ", p );
                    valid = false;
                }
                break;
            case String:
                if( typeof p.default !== 'string' && p.default !== null ) {
                    console.log( "ERROR: DEFAULT SHOULD BE A STRING/NULL FOR PROPERTY: ", p );
                    valid = false;
                }
                break;
            case Boolean:
                if( typeof p.default !== 'boolean' ) {
                    console.log( "ERROR: DEFAULT SHOULD BE A BOOLEAN FOR PROPERTY: ", p );
                    valid = false;
                }
                break;
            default:
                break;
        }
        return valid;
    }

    _validatePropertyValidValues( p ) {
        let valid = true;
        if( ( p.type === Number || p.type === String ) ) {
            if( p.validValue === undefined ) {
                console.log( "ERROR: NO VALID VALUE PROVIDED FOR PROPERTY: ", p );
                valid = false;
            }
            if( p.type === Number && typeof p.validValue !== 'number' ) {
                console.log( "ERROR: VALID VALUE SHOULD BE A NUMBER FOR PROPERTY: ", p );
                valid = false;
            } else if( p.type === String && typeof p.validValue !== 'string' ) {
                console.log( "ERROR: VALID VALUE SHOULD BE A STRING FOR PROPERTY: ", p );
                valid = false;
            }
        }
        return valid;
    }

    _buildCustomProps() {
        let lambdaBody = "return {";
        this.properties.forEach( ( p ) => {
            lambdaBody = lambdaBody + p.name + ": el => el." + p.name;
            if( p !== this.properties[this.properties.length - 1] ) {
                lambdaBody = lambdaBody + ",";
            }
        } );
        lambdaBody = lambdaBody + "};";
        let lambda = Function( lambdaBody );
        return lambda();
    } 
}