export default class AttributePropertyBehaviourIntegrationTests {

    constructor( componentModel ) {
        this._model = componentModel;
    }

    runAllTests() {
        this._runAttributePropertyBehaviourUnitTests();
    }

    runAttributePropertyBehaviourUnitTests() {
        this._runAttributePropertyBehaviourUnitTests();
    }

    _runAttributePropertyBehaviourUnitTests() {
        fixture( this._model.componentName + ' All Attribute/Property Behaviour Integration Tests' )
            .page( this._model.testServer );

        for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            switch ( prop.type ) {
                case Boolean:
                    this._testBoolean( prop.name, prop.attributeName, prop.default );
                    break;
                case String:
                    this._testString( prop.name, prop.attributeName, prop.validValue, prop.default );
                    break;
                case Number:
                    this._testNumber( prop.name, prop.attributeName, prop.validValue, prop.default );
                    break;
                default:
                    break;
            }
        }
    }

    _testBoolean( name, attrName, defaultV ) {
        test( 'Integration Tests - ' + name + ' - boolean - behaviour', async t => {
            await this._model.setComponentProperty( t, name, true );
            await this._model.testAttributeExists( t, attrName, 'Attribute "' + name + '" updates and returns property value - true' );
            await this._model.setComponentProperty( t, name, false );
            await this._model.testAttributeNotExists( t, attrName, 'Attribute "' + name + '" updates and returns property value - false' );
            await this._model.setComponentAttribute( t, attrName, true );
            await this._model.testPropertyValue( t, name, true, 'Property "' + name + '" updates and returns attribute value - true' );
            await this._model.setComponentAttribute( t, attrName, false );
            await this._model.testPropertyValue( t, name, false, 'Property "' + name + '" updates and returns attribute value - false' );
            await this._model.setComponentAttribute( t, attrName, 'true' );
            await this._model.testPropertyValue( t, name, true, 'Property "' + name + '" updates and returns attribute value - \'true\'' );
            await this._model.setComponentAttribute( t, attrName, 'false' );
            await this._model.testPropertyValue( t, name, false, 'Property "' + name + '" updates and returns attribute value - \'false\'' );
            await this._model.setComponentProperty( t, name, defaultV );
        } );               
    }

    _testString( name, attrName, valid, defaultV ) {
        test( 'Integration Tests - ' + name + ' - string - behaviour', async t => {
            await this._model.setComponentProperty( t, name, valid );
            await this._model.testAttributeValue( t, attrName, valid, 'Attribute "' + name + '" updates and returns property value - ' + valid );
            await this._model.setComponentProperty( t, name, null );
            await this._model.testAttributeNotExists( t, attrName, 'Attribute "' + name + '" updates and returns property value - null' );
            await this._model.setComponentAttribute( t, attrName, valid );
            await this._model.setComponentProperty( t, name, 'null' );  
            await this._model.testAttributeNotExists( t, attrName, 'Attribute "' + name + '" updates and returns property value - \'null\'' );   
            await this._model.setComponentProperty( t, name, defaultV );

            await this._model.setComponentAttribute( t, attrName, valid );
            await this._model.testPropertyValue( t, name, valid, 'Property "' + name + '" updates and returns attribute value - ' + valid );
            await this._model.setComponentAttribute( t, attrName, null );
            await this._model.testPropertyValue( t, name, null, 'Property "' + name + '" updates and returns attribute value - null' );
            await this._model.setComponentProperty( t, name, valid );
            await this._model.setComponentAttribute( t, attrName, 'null' );
            await this._model.testPropertyValue( t, name, null, 'Property "' + name + '" updates and returns attribute value - \'null\'' );
            await this._model.setComponentAttribute( t, attrName, defaultV );
        } );              
    }

    _testNumber( name, attrName, valid, defaultV ) {
        test( 'Integration Tests - ' + name + ' - number - behaviour', async t => {
            await this._model.setComponentProperty( t, name, valid );
            await this._model.testAttributeValue( t, attrName, valid.toString(), 'Attribute "' + name + '" updates and returns property value - ' + valid );
            await this._model.setComponentProperty( t, name, defaultV );

            await this._model.setComponentAttribute( t, attrName, valid );
            await this._model.testPropertyValue( t, name, valid, 'Property "' + name + '" updates and returns attribute value - ' + valid );
            await this._model.setComponentAttribute( t, attrName, defaultV );
            await this._model.setComponentAttribute( t, attrName, valid.toString() );
            await this._model.testPropertyValue( t, name, valid, 'Property "' + name + '" updates and returns attribute value - \'' + valid + '\'' );
            await this._model.setComponentAttribute( t, attrName, defaultV );
        } );              
    }
}