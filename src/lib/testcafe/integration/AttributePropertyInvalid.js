export default class AttributePropertyInvalidIntegrationTests {

    constructor( componentModel ) {
        this._model = componentModel;
    }

    runAllTests() {
        fixture( this._model.componentName + ' All Attribute/Property Invalid Integration Tests' )
            .page( this._model.testServer );

        this._runAttributePropertyCommonInvalidIntegrationTests();
        this._runAttributePropertyNInvalidIntegrationTests();
        this._runAttributePropertySBInvalidIntegrationTests();
    }

    runAttributePropertyCommonInvalidIntegrationTests() {
        fixture( this._model.componentName + ' Common Attribute/Property Invalid Integration Tests' )
            .page( this._model.testServer );

        this._runAttributePropertyCommonInvalidIntegrationTests();
    }

    _runAttributePropertyCommonInvalidIntegrationTests() { 
        for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            if( prop.type === Boolean || prop.type === Number || prop.type === String ) {
                test( 'Integration Tests - ' + prop.name + ' - ' + prop.attributeName + ' - common - invalid values', async t => {
                    let expected = await this._model.component[prop.name];
                    if( prop.type !== Boolean ) {
                        await this._model.setComponentAttribute( t, prop.attributeName, true );
                        await this._model.testPropertyValue( t, prop.name, expected, 'Property "' + prop.name + '" does not update and does not return invalid attribute value - boolean' );
                    }
                    if( prop.type !== Number ) {
                        await this._model.setComponentAttribute( t, prop.attributeName, 1 );
                        await this._model.testPropertyValue( t, prop.name, expected, 'Property "' + prop.name + '" does not update and does not return invalid attribute value - number' );
                    }
                    if( prop.type !== String ) {
                        await this._model.setComponentAttribute( t, prop.attributeName, 'test' );
                        await this._model.testPropertyValue( t, prop.name, expected, 'Property "' + prop.name + '" does not update and does not return invalid attribute value - string' );
                        await this._model.setComponentAttribute( t, prop.attributeName, null );
                        await this._model.testPropertyValue( t, prop.name, expected, 'Property "' + prop.name + '" does not update and does not return invalid attribute value - null' );
                    }
                    await this._model.setComponentAttribute( t, prop.attributeName, {} );
                    await this._model.testPropertyValue( t, prop.name, expected, 'Property "' + prop.name + '" does not update and does not return invalid attribute value - object' );
                    await this._model.setComponentAttribute( t, prop.attributeName, [] );
                    await this._model.testPropertyValue( t, prop.name, expected, 'Property "' + prop.name + '" does not update and does not return invalid attribute value - array' );
                    await this._model.setComponentAttribute( t, prop.attributeName, undefined );
                    await this._model.testPropertyValue( t, prop.name, expected, 'Property "' + prop.name + '" does not update and does not return invalid attribute value - undefined' );
                } );
            }
        }                     
    }

    runAttributePropertyNInvalidIntegrationTests() {
        fixture( this._model.componentName + ' Number Attribute/Property Invalid Integration Tests' )
            .page( this._model.testServer );

        this._runAttributePropertyNInvalidIntegrationTests();
    }

    _runAttributePropertyNInvalidIntegrationTests() {
        for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            if( prop.type === Number ) {
                test( 'Integration Tests - ' + prop.name + ' - ' + prop.attributeName + ' - number - invalid values', async t => {
                    let expected = await this._model.component[prop.name];
                    expected = expected.toString();
                    await this._model.setComponentProperty( t, prop.name, prop.validValue.toString() );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - valid string' );
                    await this._model.setComponentProperty( t, prop.name, 'test' );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - invalid string' );
                    await this._model.setComponentProperty( t, prop.name, null );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - null' );
                    await this._model.setComponentProperty( t, prop.name, {} );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - object' );
                    await this._model.setComponentProperty( t, prop.name, [] );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - array' );
                    await this._model.setComponentProperty( t, prop.name, undefined );
                    await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - undefined' );                           
                } );
            }
        }
    }

    runAttributePropertySBInvalidIntegrationTests() {
        fixture( this._model.componentName + ' String/Number Attribute/Property Invalid Integration Tests' )
            .page( this._model.testServer );

        this._runAttributePropertySBInvalidIntegrationTests();
    }

    _runAttributePropertySBInvalidIntegrationTests() {
        for ( let i = 0; i < this._model.properties.length; i++ ) {
            let prop = this._model.properties[i];
            if( prop.type === String || prop.type === Boolean ) {
                let type = 'string';
                if( prop.type === Boolean ) type = 'boolean';
                test( 'Integration Tests - ' + prop.name + ' - ' + prop.attributeName + ' - ' + type + ' - invalid values', async t => {
                    let expected = await this._model.component[prop.name];
                    expected = expected.toString();
                    this._testAttribute( t, prop, expected, 1, 'number' );
                    this._testAttribute( t, prop, expected, {}, 'object' );
                    this._testAttribute( t, prop, expected, [], 'array' );
                    this._testAttribute( t, prop, expected, undefined, 'undefined' );
                    if( prop.type === Boolean ) {
                        await this._model.setComponentProperty( t, prop.name, 'test' );
                        if( expected === 'true' ) {
                            await this._model.testAttributeExists( t, prop.attributeName, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - invalid string' );
                        } else {
                            await this._model.testAttributeNotExists( t, prop.attributeName, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - invalid string' );
                        }
                        let invert = !prop.default;
                        invert = invert.toString();
                        await this._model.setComponentProperty( t, prop.name, invert );
                        if( expected === 'true' ) {
                            await this._model.testAttributeExists( t, prop.attributeName, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - valid string' );
                        } else {
                            await this._model.testAttributeNotExists( t, prop.attributeName, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - valid string' );
                        }
                    } else {
                        await this._model.setComponentProperty( t, prop.name, true );
                        if( expected !== 'null' ) {
                            await this._model.testAttributeValue( t, prop.attributeName, expected, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - boolean' );
                        } else {
                            await this._model.testAttributeExists( t, prop.attributeName, 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - boolean' );
                        }
                    }
                } );
            }
        }
    }

    async _testAttribute( t, prop, expected, setTo, testType ) {
        await this._model.setComponentProperty( t, prop.name, setTo );
        let message = 'Attribute "' + prop.attributeName + '" does not update and does not return invalid property value - ' + testType;
        if( this._checkNotExisted( prop.type, expected ) ) {
            await this._model.testAttributeNotExists( t, prop.attributeName, message );
        } else {
            if( prop.type === Boolean ) {
                await this._model.testAttributeExists( t, prop.attributeName, message );
            } else {
                await this._model.testAttributeValue( t, prop.attributeName, expected, message );
            }                                
        }
    }

    _checkNotExisted( type, expected ) {
        return ( type === Boolean && expected === 'false' ) || ( type === String && expected === 'null' )
    }
}