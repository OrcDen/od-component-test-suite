const { CleanWebpackPlugin } = require('clean-webpack-plugin');
module.exports = {
    plugins: [
        new CleanWebpackPlugin()
    ],
    resolve: {
        extensions: ['.js'],
    },
    optimization: {
        minimize: false
    }
};