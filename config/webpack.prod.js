const merge = require('webpack-merge');
const common = require('./common/webpack.common');
const nodeExternals = require('webpack-node-externals');

module.exports = merge(common, {
    mode: 'production',
    entry: {
        main: './src/main.js'
    },
    output: {
        filename: 'ODComponentTestSuite.js',
        library: 'ODComponentTestSuite',
        libraryTarget: 'umd',
        globalObject: 'this'
    },
    externals: [nodeExternals()]
});