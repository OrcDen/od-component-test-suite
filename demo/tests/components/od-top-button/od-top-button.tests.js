import { TestCafeComponentTestSuite } from '../../../../src/main.js';

let config = {
    testServer: 'http://localhost:8080',
    name: 'OD Top Button',
    componentTag: 'od-top-button',
    componentSelector: '#top-button',
    hasShadow: true,
    shadowSelector: '#top-button',
    properties: [
        {
            name: 'active',
            type: Boolean,
            default: false,
            attributeName: 'active'
        },
        {
            name: 'offset',
            type: Number,
            default: 1,
            validValue: 5,
            attributeName: 'offset'
        }
    ]
}

let testSuite = new TestCafeComponentTestSuite(config);
testSuite.runAllTests();